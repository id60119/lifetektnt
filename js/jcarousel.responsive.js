(function($) {
    $(function() {
        var jcarousel = $('.jcarousel');

        jcarousel
            .on('jcarousel:reload jcarousel:create', function () {
                var carousel = $(this),
                    org_width = carousel.innerWidth();

                var total = 1;
                var fix = 1;
                var width = 0;

                total = parseInt($(this).attr('max'));

                if($(this).hasClass('max'))
                    fix = 0;

                if(total > 0)
                {
                    width = org_width / total;
                }
                else
                {
                    width = org_width / 2;
                }

                if(fix == 1)
                {
                    width = org_width / total;
                }

                if(org_width <= 767)
                    width = org_width / 2;


                console.log('width ' + width);

                carousel.jcarousel('items').css('width', Math.ceil(width) + 'px');
            })
            .jcarousel({
                wrap: 'circular'
            });

        $('.jcarousel-control-prev')
            .jcarouselControl({
                target: '-=1'
            });

        $('.jcarousel-control-next')
            .jcarouselControl({
                target: '+=1'
            });

        $('.jcarousel-pagination')
            .on('jcarouselpagination:active', 'a', function() {
                $(this).addClass('active');
            })
            .on('jcarouselpagination:inactive', 'a', function() {
                $(this).removeClass('active');
            })
            .on('click', function(e) {
                e.preventDefault();
            })
            .jcarouselPagination({
                perPage: 1,
                item: function(page) {
                    return '<a href="#' + page + '">' + page + '</a>';
                }
            });
    });
})(jQuery);